#!/usr/bin/env python3
import argparse
import logging
from decimal import Decimal

from gnucash import GncNumeric
from gnucash import Session
from gnucash import Split
from gnucash import Transaction

DEFAULT_ACCOUNT_1 = "Assets:Current Assets:Cash in Wallet"
DEFAULT_ACCOUNT_2 = "Expenses:Dining"


def lookup_account_by_path(root, path):
    acc = root.lookup_by_name(path[0])
    if acc.get_instance() is None:
        raise Exception("Account path {} not found".format(":".join(path)))
    if len(path) > 1:
        return lookup_account_by_path(acc, path[1:])
    return acc


def lookup_account(root, name):
    path = name.split(":")
    return lookup_account_by_path(root, path)


def get_full_account_name(account):
    if account:
        parent_full_name = get_full_account_name(account.get_parent())
        # do not add "Root Account"
        if parent_full_name and account.get_parent().get_parent():
            return parent_full_name + ":" + account.name
        else:
            return account.name
    else:
        return None


def to_gnc_numeric(amount, currency, positive=1):
    item_amount = Decimal(amount.replace(",", "."))
    amount = int(item_amount * currency.get_fraction())

    return GncNumeric(amount * positive, currency.get_fraction())


def item_already_in_book(account, description, date, amount, currency):
    """Find transaction by description than check date and amount."""

    for split in account.GetSplitList():
        trans = split.parent
        trans_date = trans.GetDate()
        if (
            trans_date.strftime("%Y-%m-%d") == date
            and trans.GetDescription() == description
        ):
            tx_amount = trans.GetAccountAmount(account)
            if tx_amount.equal(to_gnc_numeric(amount, currency, positive=1)):
                return True

    return False


def get_description_mapping(account):
    mapping = {}
    for split in account.GetSplitList():
        trans = split.parent
        other_split = split.GetOtherSplit()
        if other_split:
            mapping[trans.GetDescription()] = get_full_account_name(other_split.account)
    return mapping


def parse_transactions_from_markdown_file(
    markdown_file, currency, description_mapping, default_account2
):
    currency_symbol = currency.get_nice_symbol()
    date = None
    with open(markdown_file) as fd:
        for line in fd:
            if line.startswith("#"):
                title = line.lstrip("#").strip()
                date = title
            else:
                words = line.strip().split(None, 1)
                if len(words) == 2 and currency_symbol in words[0]:
                    amount = words[0].strip(currency_symbol)
                    if amount.startswith("+"):
                        amount = amount.lstrip("+")
                    else:
                        amount = "-" + amount
                    description = words[1]
                    yield {
                        "amount": amount,
                        "account_2": description_mapping.get(
                            description, default_account2
                        ),
                        "description": description,
                        "date": date,
                    }


def add_transaction(book, date, account_1, account_2, amount, currency, description):
    logging.info(f"Adding transaction {date} {amount} {description}..")

    tx = Transaction(book)
    tx.BeginEdit()
    tx.SetCurrency(currency)
    year, month, day = map(int, date.split("-"))
    tx.SetDate(day, month, year)
    tx.SetDescription(description)

    s1 = Split(book)
    s1.SetParent(tx)
    s1.SetAccount(account_1)
    amount = int(Decimal(amount.replace(",", ".")) * currency.get_fraction())
    s1.SetValue(GncNumeric(amount, currency.get_fraction()))
    s1.SetAmount(GncNumeric(amount, currency.get_fraction()))

    s2 = Split(book)
    s2.SetParent(tx)
    s2.SetAccount(account_2)
    s2.SetValue(GncNumeric(amount * -1, currency.get_fraction()))
    s2.SetAmount(GncNumeric(amount * -1, currency.get_fraction()))

    tx.CommitEdit()


def import_transactions(
    markdown_files, gnucash_file, from_date, dry_run, default_account1, default_account2
):

    logging.debug("Opening GnuCash file %s..", gnucash_file)
    session = Session(gnucash_file)
    try:
        book = session.book
        root = book.get_root_account()
        account = lookup_account(root, default_account1)
        currency = account.GetCommodity()

        description_mapping = get_description_mapping(account)

        for markdown_file in markdown_files:
            for transaction in parse_transactions_from_markdown_file(
                markdown_file, currency, description_mapping, default_account2
            ):
                logging.debug(f"Found transaction {transaction}")
                if not item_already_in_book(
                    account,
                    transaction["description"],
                    transaction["date"],
                    transaction["amount"],
                    currency,
                ) and (not from_date or transaction["date"] >= from_date):
                    add_transaction(
                        book,
                        transaction["date"],
                        account,
                        lookup_account(root, transaction["account_2"]),
                        transaction["amount"],
                        currency,
                        transaction["description"],
                    )

        if not dry_run:
            logging.info(f"Saving GnuCash file {gnucash_file}..")
            session.save()

    finally:
        session.end()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--dry-run",
        action="store_true",
        help="Dry-run mode: do not save changes to GnuCash file",
    )
    parser.add_argument("--from-date", help="Only process transactions since")
    parser.add_argument(
        "-v", "--verbose", action="store_true", help="Enable debug logging"
    )
    parser.add_argument(
        "markdown_file", nargs="+", help="Markdown files with transactions to import"
    )
    parser.add_argument(
        "gnucash_file", help="GnuCash data file where transactions should be saved"
    )
    parser.add_argument(
        "--default-account1", help="Default cash account", default=DEFAULT_ACCOUNT_1
    )
    parser.add_argument(
        "--default-account2", help="Default expense account", default=DEFAULT_ACCOUNT_2
    )
    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO)

    import_transactions(
        args.markdown_file,
        args.gnucash_file,
        args.from_date,
        args.dry_run,
        args.default_account1,
        args.default_account2,
    )


if __name__ == "__main__":
    main()
