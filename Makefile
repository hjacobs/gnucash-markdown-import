.PHONY: test docs package upload

VERSION          ?= $(shell git describe --tags --always --dirty)

default: package

clean:
	rm -fr build dist *egg-info .tox/ .cache/ .pytest_cache/ docs/_build/

.PHONY: install
install:
	poetry install

.PHONY: lint
lint: install
	poetry run pre-commit run --all-files

test: lint install
	poetry run coverage run --source=gnucash_markdown_import -m pytest
	poetry run coverage html
	poetry run coverage report

package: test
	poetry build

upload: package
	poetry publish

version:
	sed -i 's/__version__ = .*/__version__ = "${VERSION}"/' gnucash_markdown_import/__init__.py
	poetry version "${VERSION}"
