# GnuCash Markdown Import

Import [GnuCash](https://gnucash.org/) transactions from Markdown files. I created this script to track cash expenses on my mobile phone using [Markor](https://f-droid.org/packages/net.gsantner.markor/) with simple journal entries like:


```
# 2022-11-29

1.50€ Ice cream
4.80€ Coffee

# 2022-11-30

32€ Groceries
+60€ John paid back
```

The default account is `Assets:Current Assets:Cash in Wallet`, the default expense account is `Expenses:Dining`. Change the default accounts via `--default-account1` and `--default-account2` command line options.
A `+` prefix for amounts will reverse the transaction direction, i.e. treat it as money received.
Transactions will only be imported once (matching date, description, and amount) when running the import multiple time.

The Markdown (`.md`) files are synched to my desktop/laptop via [Syncthing](https://f-droid.org/en/packages/com.nutomic.syncthingandroid/) and this script imports the transactions into GnuCash:

```
# using system Python3 as it has the GnuCash Python bindings (apt install python3-gnucash)
/usr/bin/python3 -m gnucash_markdown_import ~/Documents/Markor/*.md ~/Documents/Accounts.gnucash
```
